import { Component } from '@angular/core';
import {Md5} from 'ts-md5/dist/md5';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent {
  title = 'md5-project';

  constructor(){}

  ngOnInit(){}

  message: string;

  messageEncrypted: any;

  encrypt(): void{
    console.warn(this.message);
    const md5 = new Md5();
    this.messageEncrypted = md5.appendStr(this.message).end();
  }
}
